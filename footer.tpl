<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="footer__logo-wrapper">
                    <img src="img/logo-light.png" alt="Forx" class="footer__logo">
                </div>
            </div>
            <div class="col-md-8">
                <div class="footer__city">
                    <div class="city">
                        <ul class="city__list">
                            <li class="city__list-item">
                                <a href="#" class="city__link">Киев</a>
                            </li>
                            <li class="city__list-item">
                                <a href="#" class="city__link">Одесса</a>
                            </li>
                            <li class="city__list-item">
                                <a href="#" class="city__link">Днепропетровск</a>
                            </li>
                            <li class="city__list-item">
                                <a href="#" class="city__link">Харьков</a>
                            </li>
                            <li class="city__list-item">
                                <a href="#" class="city__link">Львов</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-lg-4">
                <div class="footer__partners-title-wrapper">
                    <h6 class="footer__partners-title">Аффилированные партнеры:</h6>
                </div>
            </div>
            <div class="col-xs-12 col-lg-8">
                <div class="footer__partners">
                    <div class="partners">
                        <ul class="partners__list">
                            <li class="partners__list-item">
                                <a href="#" class="partners__link">
                                    <div class="partners__image-overlay">
                                        <div class="partners__image-wrapper">
                                            <img src="images/partners/1.png" alt="" class="partners__image">
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="partners__list-item">
                                <a href="#" class="partners__link">
                                    <div class="partners__image-overlay">
                                        <div class="partners__image-wrapper">
                                            <img src="images/partners/2.png" alt="" class="partners__image">
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="partners__list-item">
                                <a href="#" class="partners__link">
                                    <div class="partners__image-overlay">
                                        <div class="partners__image-wrapper">
                                            <img src="images/partners/3.png" alt="" class="partners__image">
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="partners__list-item">
                                <a href="#" class="partners__link">
                                    <div class="partners__image-overlay">
                                        <div class="partners__image-wrapper">
                                            <img src="images/partners/4.png" alt="" class="partners__image">
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-lg-offset-4">
                <div class="footer__powered-wrapper">
                    <span class="footer__powered">Сайт разработан в студии Abweb <a href="//abweb.com.ua">abweb.com.ua</a></span>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="lib/jquery-3.2.1.min.js"></script>
<script src="lib/jquery-ui.min.js"></script>
<script src="lib/slick/slick.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>
