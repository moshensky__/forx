<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Forx</title>

    <!-- Css styles -->
    <link href="lib/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="lib/jquery-ui.min.css" rel="stylesheet">
    <link href="lib/slick/slick.css" rel="stylesheet">
    <link href="lib/animate.css" rel="stylesheet">
    <link href="lib/hamburgers.min.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--SVG-->
<div style="display: none;">
    <!-- partial:img/svg/svg-sprite/symbol/svg/sprite.symbol.svg -->
    <!-- partial -->
</div>
<!--/SVG-->

<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-xs-9 col-sm-4 col-md-3 col-lg-2">
                <div class="logo__wrapper">
                    <img src="img/logo.png" alt="Forx. Ваш груз на нужной высоте" class="logo">
                    <span class="logo__slogan">Ваш груз на нужной высоте</span>
                </div>
            </div>
            <div class="col-lg-6 header__hamburger-content">
                <div class="header__nav">
                    <nav class="menu">
                        <ul class="menu__list">
                            <li class="menu__list-item">
                                <a href="#" class="menu__link">О компании</a>
                            </li>
                            <li class="menu__list-item">
                                <a href="#" class="menu__link">Каталог</a>
                            </li>
                            <li class="menu__list-item">
                                <a href="#" class="menu__link">Сервисное обслуживание</a>
                            </li>
                            <li class="menu__list-item">
                                <a href="#" class="menu__link">Медиа центр</a>
                            </li>
                            <li class="menu__list-item">
                                <a href="#" class="menu__link">Контакты</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <div class="col-sm-7 col-md-5 col-md-offset-3 col-lg-offset-0 col-lg-4 header__attr">
                <div class="header__tel">
                    <select name="tel" id="select-tel" class="header__tel-select">
                        <option value="Киев">+3<br>8 (044) 467-26-04</option>
                        <option value="Харьков">+38 (057) 467-26-04</option>
                        <option value="МТС">+38 (050) 467-26-04</option>
                        <option value="Lifecell">+38 (063) 467-26-04</option>
                    </select>
                </div>
                <div class="header__lang">
                    <select name="lang" id="select-lang" class="header__lang-select">
                        <option value="ua">ukraine</option>
                        <option value="ru">russia</option>
                        <option value="uk">uk</option>
                    </select>
                </div>
                <a href="#" class="header__social">
                    <img src="img/youtube.png" alt="Youtube" class="header__social-image">
                </a>
            </div>
            <div class="col-xs-3 col-sm-1 pull-right header__hamburger">
                <button class="hamburger hamburger--collapse" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
</header>