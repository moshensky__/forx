function selectmenu(){
    $.widget( "custom.iconselectmenu", $.ui.selectmenu, {
        _renderItem: function( ul, item ) {
            var li = $( "<li>" ),
                wrapper = $("<div>", {
                    "class": "header__tel-item-wrapper"
                });
            $("<span>", {
                text: item.element.attr( "value" ),
                "class": "header__tel-item-city"
            }).appendTo(wrapper);
            $( "<span>", {
                "class": "header__tel-item-icon"
            }).appendTo( wrapper );
            $( "<span>", {
                text: item.label,
                "class": "header__tel-item-number"
            }).appendTo( wrapper );

            return li.append( wrapper ).appendTo( ul );
        },
        _renderButtonItem: function(item){
            var buttonItem = $( "<div>", {
                "class": "header__tel-selected-wrapper"
            });
            $("<span>", {
                text: item.element.attr( "value" ),
                "class": "header__tel-selected-city"
            }).appendTo(buttonItem);
            $( "<span>", {
                "class": "header__tel-selected-icon"
            }).appendTo(buttonItem);
            $( "<span>", {
                text: item.label,
                "class": "header__tel-selected-number"
            }).appendTo(buttonItem);

            return buttonItem;
    }
    });
    $.widget( "custom.langselectmenu", $.ui.selectmenu, {
        _renderItem: function( ul, item ) {
            var li = $( "<li>",{
                "class": "header__lang-item-wrapper"
            } );


            $("<span>", {
                html: '<svg><use xlink:href="#'+item.label+'"></use></svg>',
                "class": "header__lang-item"
            }).appendTo(li);


            return li.appendTo( ul );
        },
        _renderButtonItem: function(item){
            var buttonItem = $( "<div>", {
                "class": "header__lang-selected-wrapper"
            });
            $("<span>", {
                html: '<svg><use xlink:href="#'+item.label+'"></use></svg>',
                "class": "header__lang-selected"
            }).appendTo(buttonItem);

            return buttonItem;
        }
    });
    if($('#select-tel').length){
        $('#select-tel').iconselectmenu();
    }
    if($('#select-lang').length){
        $('#select-lang').langselectmenu().langselectmenu( "menuWidget" ).addClass( "header__lang-wrapper" );
    }
}

function slider(){
    if($('.js-slider').length){
        $('.js-slider').slick({
            autoplay: true,
            autoplaySpeed: 3000,
            arrows: false,
            dots: true,
            customPaging: function (slider, i){
                return "<button class='btn slider__dots'></button>";
            }
        });
    }
    if($('#js-news-slider').length){
        $('#js-news-slider').slick({
            prevArrow: $('.news__slider-nav--prev'),
            nextArrow: $('.news__slider-nav--next'),
            slidesToShow: 2,
            responsive:[
                {
                    breakpoint: 1200,
                    settings: {
                        slidesToShow: 1,
                    }
                }
            ]
        });
    }
}

function video(){
    var video = $('#js-video');
    if(video){
        $('#js-video-play').click(function (){
            if(video[0].paused === true){
                $(this).addClass('video__play--hidden');
                video[0].play();
            }
        });
        video.on('play', function (){
            video.attr('controls', 'true');
        });
    }
}

function softScroll(){
    $('a.anchor').click(function (){
        var elementClick = $(this).attr("href");
        var destination = $(elementClick).offset().top;
        $('html,body').animate({
            scrollTop: destination
        }, 800);
        return false;
    });
}

function hamburger(){
    $('.hamburger').click(function (){
        if($(this).hasClass('is-active')){
            $(this).removeClass('is-active');
            $('.header__hamburger-content').slideUp();
        }else{
            $(this).addClass('is-active');
            $('.header__hamburger-content').slideDown();
        }
    });
}

jQuery(document).ready(function($) {
    selectmenu();
    slider();
    video();
    softScroll();
    hamburger();
});
